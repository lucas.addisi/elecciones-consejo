$(function(){
	console.log("Iniciando web socket");
	
	var webSocket = new WebSocket("ws://"+ window.location.hostname +":"+ window.location.port +"/ws/lista-votos-hash");
	
	$("#ordenDelDiaField").keyup(function(e){
		var textValue = $("#ordenDelDiaField").val();
		var itemOrdenDia = {itemName : textValue}
		webSocket.send(JSON.stringify(itemOrdenDia));		
	})
	
	webSocket.onmessage = function (event) {
		$("#hashField").text(event.data)
	}
	
	$("#nextButton").click(function(){
		window.location.replace("http://" + window.location.hostname + ":" + window.location.port + "/validador/next-page")
		
	})
	
});
