$(function(){
	var row
	var webSocket = new WebSocket("ws://"+ window.location.hostname +":"+ window.location.port +"/ws/orden-del-dia-hash");
	var rowNumber= 1
	
	var cleanFormTexts = function() {
		$("#name-form"+rowNumber).prop('disabled',true)
		$("#vote-selected"+rowNumber).prop('disabled',true)
	}

	
	var addRow = function(){
		$('#vote-table tr:last').after("<tr><th scope=\"row\">"+rowNumber+"</th><th><div><input type=\"text\" class=\form-control\" id=\"name-form"+rowNumber+"\" placeholder=\"Nombre y Apellido\">" +
		"</div></th><th><div><select class=\"custom-select mr-sm-2\" id=\"vote-selected"+rowNumber+"\"><option selected>Voto</option>"+
		"<option value=\"1\">Afirmativo</option>"+
		"<option value=\"2\">Negativo</option>" +
		"<option value=\"3\">Abstención</option>"+
		"</select></div></th><th><div><label for=\"ordenDelDiaText\">Hash Code</label></div></th></tr>")

	}
	
	var generateHash = function(){
		var nameAndSurname = $("#name-form"+rowNumber).val()
		var voteValue = $("#vote-selected" + rowNumber + " option:selected").text();
		var vote = {name_surname : nameAndSurname, vote_value: voteValue}
		webSocket.send(JSON.stringify(vote));
	}

	webSocket.onmessage = function (event) {
		console.log(event.data)
	}
	
	$("#add-voter").click(function(e) {
		generateHash();
		cleanFormTexts();
		rowNumber++;
		addRow();
			
	});
	
	$("#nextButton").click(function(){
		window.location.replace("http://" + window.location.hostname + ":" + window.location.port + "/validador/next-page")
	})
	
	$("#previousButton").click(function(){
		window.location.replace("http://" + window.location.hostname + ":" + window.location.port + "/validador/previous-page")
	})
	
	addRow();
	
});