from django.shortcuts import render
from django.views.decorators.cache import cache_control
from django.http import HttpResponse
from web_view.view_selector import NavigationController


nav_controller = NavigationController()

# Create your views here.
def index(request):
    index_view = nav_controller.get_index_view();
    return render(request, index_view + ".html")


def next_view(request):
    next_view = nav_controller.next_view()
    return render(request, next_view + ".html")


def previous_view(request):
    previous_view = nav_controller.previous_view()
    return render(request, previous_view + ".html")