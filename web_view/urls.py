from django.urls import path 
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('next-page',views.next_view, name="Next View"),
    path('previous-page',views.previous_view, name="Previous View"),
]
    