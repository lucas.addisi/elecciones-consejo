from django.apps import AppConfig


class WebviewConfig(AppConfig):
    name = 'web_view'
