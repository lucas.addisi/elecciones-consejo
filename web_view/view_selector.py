import json

class NavigationController:
    
    def __init__(self):
        self.views_position = self.__read_config_file('web_view/views_positioning.json')
        self.ptr = Pointer()

    def get_index_view(self):
        self.ptr.relocate_ptr(0)
        print("El puntero está en la posición", self.ptr)
        return self.views_position[0]
    
    def next_view(self):       
                #Si no se pasa del largo
        if (self.ptr.get_value() + 1 <= len(self.views_position) and self.ptr.get_value() + 1 >= 0):
            self.ptr.increase_ptr()
            print("El puntero está en la posición: " + str(self.ptr))
            next_view = self.views_position[self.ptr.get_value()]
            return next_view
        else:
            print("Array out of bounds")
            #Raise error
        
    
    def previous_view(self):
        if (self.ptr.get_value() - 1 <= len(self.views_position) and self.ptr.get_value() - 1 >= 0):
            self.ptr.decrease_ptr()
            print("El puntero está en la posición: " + str(self.ptr))
            previous_view = self.views_position[self.ptr.get_value()]
            print(previous_view)
            return previous_view
        else:
            print("Array out of bounds")
            #Raise error
    
    def __read_config_file(self,file_name):
        with open(file_name, 'r') as data:
            config = json.load(data)
        return config
    

class Pointer:
    
    def __init__(self):
        self.__ptr = 0
        
    def relocate_ptr(self, index):
        self.__ptr = index
        
    def increase_ptr(self):
        self.__ptr = self.__ptr + 1
        
    def decrease_ptr(self):
         self.__ptr = self.__ptr - 1
         
    def get_value(self):
        return self.__ptr
         
    def __str__(self):
        return str(self.__ptr)