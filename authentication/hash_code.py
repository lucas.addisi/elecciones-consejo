import hashlib

class HashCodeGenerator:
    
    def __init__(self, hash_algorithm):
        if(hash_algorithm.lower() == "sha1"):
            self.hash_algorithm = "sha1"
        else:
            raise ValueError("Algorithm " + hash_algorithm + " is invalid")
        
    def generate_hash(self, data):
        hash_generator = hashlib.new(self.hash_algorithm, data.encode())
        return hash_generator.hexdigest()
    
    def generate_composed_hash(self, data1, data2):
        hash1 = self.generate_hash(data1)
        hash2 = self.generate_hash(data2)
        
        return hash1 * hash2 


class StringHashCode:
    
    def __init__(self):
        self.hash_generator = HashCodeGenerator("sha1")
    
    def get_string_hash(self, data):
        if (data == ""):
            return None
        else:
            return self.hash_generator.generate_hash(data)