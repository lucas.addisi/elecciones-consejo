from channels.generic.websocket import WebsocketConsumer
from authentication.hash_code import StringHashCode
from authentication.models import Voto, ItemOrdenDia, Votacion
import json

class OrdenDelDiaConsumer(WebsocketConsumer):
    
    def connect(self):
        print("Conectado a Orden de día Consumer")
        self.hash_coder = StringHashCode()
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        data = json.loads(text_data)
        ItemOrdenDia(data["itemName"])
        
class ListaVotosConsumer(WebsocketConsumer):    
    
    def connect(self):
        print("Conectado a Lista Votos Consumer")
        self.hash_coder = StringHashCode()
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        data = json.loads(text_data)
        Voto(data["name_surname"], data["vote_value"])
