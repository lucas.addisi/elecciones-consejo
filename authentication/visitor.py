from authentication.hash_code import hash_code

class HashGeneratorVisitor:
    
    def __init__(self):
        self.hash_coder = HashCodeGenerator("sha1")

    
    def visit_data_element(self, element): #TODO sería bueno nombrar al elemento visitado de una mejor manera
        return self.hash_coder.generate_hash(element)