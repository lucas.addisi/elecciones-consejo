from django.conf.urls import url

from . import hash_generator_consumer

websocket_url = [
        url("ws/orden-del-dia-hash", hash_generator_consumer.OrdenDelDiaConsumer),
        url("ws/lista-votos-hash", hash_generator_consumer.ListaVotosConsumer),
    ]