from django.db import models

# Create your models here.

class ItemOrdenDia(models.Model):
    
    def __init__(self, item_name):
        self.item_name = item_name

class Voto(models.Model):
    
    def __init__(self, nombre_apellido, eleccion_voto):
        self.nombre_apellido = nombre_apellido
        self.voto = eleccion_voto
        

class Votacion(models.Model):
    
    def __init__(self, item_orden_dia, votos):
        self.item_orden_dia = item_orden_dia
        self.votos = votos
    